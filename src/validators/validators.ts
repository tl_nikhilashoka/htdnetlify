export const validators = {

    nameValidator (value: string) : boolean {
        if (/^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/.test(value)) {
            return true
        } else {
            return false
        }
    },
    
    numberValidator(value: string) : boolean {
        if (/^\d{10}$/.test(value)) {
            return true
        } else {
            return false
        }
    },
    
    emailValidator(value: string) : boolean {
        if (/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/.test(value)) {
            return true
        } else {
            return false
        }
    },
    
    textValidator(value: string) : boolean {
        if (/^[a-zA-Z]*$/.test(value)) {
            return true
        } else {
            return false
        }
    },
    
    lastHundredYears(value: string) : boolean {
        if (parseFloat(value) >= 1920 && parseFloat(value) < 2021) {
            return true
        } else {
            return false
        }
    },
    
    percentageValidator(value: string) : boolean {
        if (parseFloat(value) >= 0 && parseFloat(value) < 101) {
            return true
        } else {
            return false
        }
    }
}