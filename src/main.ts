import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import vuetify from './plugins/vuetify'
import setUpInterceptor from '@/http/interceptor'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret, faUpload, faCheck, faPlus, faCheckCircle, faTimesCircle, faListAlt, faTimes, faPlusCircle, faUserPlus, faClipboardList, faFilter} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
 
library.add(faUserSecret)
library.add(faUpload)
library.add(faCheck)
library.add(faPlus)
library.add(faCheckCircle)
library.add(faTimesCircle)
library.add(faListAlt)
library.add(faTimes)
library.add(faPlusCircle)
library.add(faUserPlus)
library.add(faClipboardList)
library.add(faFilter)


Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false


 
// import Keycloak from 'keycloak-js'


 
// createApp(App).use(store).use(router).mount('#app')

setUpInterceptor()
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

 
// const initOptions = {
//     url: 'http://127.0.0.1:8080/auth', realm: 'esslite-realm', clientId: 'htd', onLoad: "login-required"
//   }
  
//   const keycloak = Keycloak(initOptions);
  
//   keycloak.init({ onLoad: "login-required" }).then((auth) => {
//     if (!auth) {
//         window.location.reload();
//     } else {
//       new Vue({
//         router,
//         store,
//         vuetify,
//         render: h => h(App, { props: { keycloak: keycloak } })
//       }).$mount('#app')
      
//         //   Vue.$log.info("Authenticated");
//     }
    
    
//   //Token Refresh
//     setInterval(() => {
//       keycloak.updateToken(70).then((refreshed) => {
//         if (refreshed) {
//         //   Vue.$log.info('Token refreshed' + refreshed);
//         } else {
//         //   Vue.$log.warn('Token not refreshed, valid for '
//         //     + Math.round(keycloak.tokenParsed.exp + keycloak.timeSkew - new Date().getTime() / 1000) + ' seconds');
//         }
//       }).catch(() => {
//         // Vue.$log.error('Failed to refresh token');
//       });
//     }, 6000)
  
//   }).catch(() => {
//     // Vue.$log.error("Authenticated Failed");
//   });