export const review = ['Excellent', 'Good', 'Average', 'Below Average']
export const interviewFeedback = [
  {
    heading: "Communication Skill",
    value: "",
    keyVal: "communicationSkill",
    dropDownValue: review
  },
  {
    heading: "Presentation",
    value: "",
    keyVal: "presentationSkill",
    dropDownValue: review
  },
  {
    heading: "Learning Ability",
    value: "",
    keyVal: "learningAbility",
    dropDownValue: review
  },
  {
    heading: "Technical Skill",
    value: "",
    keyVal: "technicalSkill",
    dropDownValue: review
  },
  {
    heading: "Interview Taken By",
    value: "",
    keyVal: "interviewer",
    dropDownValue: review
  },
];