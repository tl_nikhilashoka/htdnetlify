export const singleCandidateData = {
    candidateId: '',
    serialNo: '',
    name: '',
    contactNumber: '',
    email: '',
    degree: '',
    stream: '',
    yop: 1996,
    tenthPercentage: 50,
    twelfthPercentage: 50,
    degreePercentage: 60,
    masterDegreePercentage: 40,
    branch: ''
}

export const candidateDataObj = {
    name: "pradyum",
    contactNumber: "7676774797",
    emailId: "pradyum.w@testyantra.in",
    degree: "BE",
    stream: "computer science and engineering",
    yop: 2020,
    tenth: 85,
    twelfth: 65,
    degreeMarks: 59,
    status: "addressing",
    jspidersBranch: "rajajinagar",
    masterDegreeAggregate: 56,
    lastUpdatedAt: null,
    lastUpdatedBy: null
}
