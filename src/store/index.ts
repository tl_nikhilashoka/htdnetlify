import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loading: false,
    popUpObj: {
    isPopUpVisible: false,
    popUpType: 'error',
    popUpMessage: ''
    }
  },
  mutations: {
    updatePopUp(state, payload) {
      state.popUpObj.isPopUpVisible = payload.isPopUpVisible,
      state.popUpObj.popUpType = payload.popUpType
      state.popUpObj.popUpMessage = payload.popUpMessage
    },
    updateLoading(state, payload) {
      state.loading = payload.value
    },
    updateIsPopUpVisible(state, payload) {
      state.popUpObj.isPopUpVisible = payload
    },
  },
  actions: {
    updatePopUp(context, payload) {
      context.commit('updatePopUp', payload)
    },
    updateLoading(context, payload) {
      context.commit('updateLoading', payload)
    },
    updateIsPopUpVisible(context, payload) {
      context.commit('updateIsPopUpVisible', payload)
    },
  },
  modules: {
  }
})
