export const candidateData = {
    candidateId: '',
    serialNo: '',
    name: '',
    contactNumber: '',
    email: '',
    degree:'',
    stream:'',
    yop: 1996,
    tenthPercentage: 50,
    twelfthPercentage: 50,
    degreePercentage: 60,
    masterDegreePercentage: 40,
    jspidersBranch: ''
   }