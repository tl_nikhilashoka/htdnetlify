import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

Vue.use(VueRouter)
Vue.mixin({
  beforeRouteUpdate(to, from, next) {
    if (from.name === 'Analytics') {
      next({ name: 'Round' })
    }
  }
})
const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Analytics',
    component: () => import('../components/Analytics.vue'),
  },
  {
    path: '/list',
    name: 'list',
    component: () => import('../components/List.vue'),
    props: true,
    beforeEnter: (to, from, next) => {
      if(from.name === 'Analytics'){
        next()
      } else {
        next({name: 'Analytics'})
      }
    }
  },
  {
    path: '/fileupload',
    name: 'File Upload',
    component: () => import('../components/FileUpload.vue'),
    props: true,
  },
  {
    path: '/round',
    name: 'Round',
    component: () => import('../components/Round.vue'),
    props: true,
    beforeEnter: (to, from, next) => {
      if(from.name === 'Analytics'){
        next()
      } else if(from.name === 'list'){
        next()
      } else {
        next({name: 'Analytics'})
      }
    }
  }, {
    path: '/interviewfeedback',
    name: 'InterviewFeedback',
    component: () => import('../components/InterviewFeedback.vue'),
    beforeEnter: (to, from, next) => {
      if(from.name === 'List'){
        next()
      } else {
        next({name: 'Analytics'})
      }
    }
  },
  {
    path: '/*',
    redirect: { name: 'Analytics' }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
