import axios from 'axios';

export const HTTP = axios.create({
  baseURL: `https://htdportal-app.herokuapp.com/api/`
})
