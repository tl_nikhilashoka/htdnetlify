import { HTTP } from './http-common'
import store from '../store/index'

function setUpInterceptor() {
  HTTP.interceptors.request.use(
    config => {
      store.dispatch('updateLoading', { value: true })
      console.log('config', config)
      console.log(store.state.loading);
      return config;
    },
    error => {
      store.dispatch('updateLoading', { value: true })
      store.dispatch('updatePopUp', {
        isPopUpVisible: true,
        popUpType: "error",
        popUpMessage: "Found Error",
      });
      return Promise.reject(error);
    }
  );
  HTTP.interceptors.response.use(
    response => {
      console.log('response', response)
      store.dispatch('updateLoading', { value: false })
      /** TODO: Add any response interceptors */
      return response;
    },
    error => {
      store.dispatch('updateLoading', { value: false })
      store.dispatch('updatePopUp', {
        isPopUpVisible: true,
        popUpType: "error",
        popUpMessage: "Found Error",
      });
      return Promise.reject(error);
    }
  );
}

export default setUpInterceptor